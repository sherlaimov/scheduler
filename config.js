const rc = require("rc");

const APP_NAME = "SCHEDULER";

module.exports = rc(APP_NAME, {
  port: 5000,
  secret: "supersecret",
  db: {
    client: "sqlite3",
    connection: {
      filename: "./test.sqlite"
    },
    debug: true
  }
});

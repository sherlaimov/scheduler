const express = require("express");
const bodyParser = require("body-parser");
const path = require("path");
const cors = require("cors");
const fs = require("fs");
const util = require("util");
const qs = require("qs");

const readFile = util.promisify(fs.readFile);
// server.js
const jsonServer = require("json-server");

const router = jsonServer.router("db.json");
const middlewares = jsonServer.defaults();

const config = require("./config");

// IMPORT MODELS
// require("./models/Product");

const app = express();

app.use(cors());

app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    // to support URL-encoded bodies
    extended: true
  })
);

// IMPORT ROUTES
// require("./routes/productRoutes")(app);

app.get("/ping", (req, res) => {
  return res.status(200).send("pong");
});

app.post("/login", (req, res) => {
  const { email, password } = req.body;

  readFile("./db.json").then(buf => {
    const data = JSON.parse(buf.toString());
    const found = data.users.find(user => {
      return user.email === email && user.password === password;
    });
    if (found) {
      res.status(200).send({
        success: true,
        user: found
      });
      return;
    }
    res.status(404).send({ success: false });
  });
});

app.use(middlewares);
app.use(router);

if (process.env.NODE_ENV === "production") {
  app.use(express.static("client/build"));

  app.get("*", (req, res) => {
    res.sendFile(path.resolve(__dirname, "client", "build", "index.html"));
  });
}

const PORT = config.PORT || 5000;
app.listen(PORT, () => {
  global.console.log(`app running on port ${PORT}`);
});

import React, { useEffect, useState } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import Home from './components/Home';
import SignUp from './components/signup/Signup';
import Login from './components/login/Login';
import NavBar from './components/navbar/Navbar';
import Eventboard from './components/eventboard/Eventboard';
import ProtectedRoute from './components/helpers/ProtectedRoute';

import './normalize.css';
import './main.scss';

class App extends React.Component {
  state = {
    isLoggedIn: false,
    user: null,
  };

  isLoggedIn() {
    const user = window.localStorage.getItem('user');
    if (user) {
      this.setState(() => ({ isLoggedIn: true, user: JSON.parse(user) }));
    } else {
      this.setState((state, props) => {
        return { isLoggedIn: false };
      });
    }
  }

  handleLogout = () => {
    window.localStorage.removeItem('user');
    this.setState({ isLoggedIn: false, user: null });
  };
  handleLogin = user => {
    this.setState({ isLoggedIn: true, user });
    //TODO Why this.props is an empty object here & I cannot redirect?
    // this.props.history.push("/eventboard");
  };
  componentDidMount() {
    this.isLoggedIn();
  }

  render() {
    const { user, isLoggedIn } = this.state;
    console.log(user);
    return (
      <Router>
        <NavBar isLoggedIn={isLoggedIn} handleLogout={this.handleLogout} />
        <Switch>
          <Route
            exact
            path="/"
            render={props => (
              <Home {...props} component={Home} isLoggedIn={isLoggedIn} user={user} />
            )}
          />
          <Route
            path="/signup"
            render={props => <SignUp {...props} handleLogin={this.handleLogin} />}
          />
          <Route
            path="/login"
            render={props => <Login {...props} handleLogin={this.handleLogin} />}
          />
          <ProtectedRoute path="/eventboard" component={Eventboard} isLoggedIn={isLoggedIn} />
        </Switch>
      </Router>
    );
  }
}

export default App;

import React from 'react';
import { Link, NavLink } from 'react-router-dom';

import './navbar.scss';

const NavBar = ({ isLoggedIn, handleLogout }) => (
  <nav className="navbar">
    <h1>
      <NavLink exact to="/">
        Home
      </NavLink>
    </h1>
    <ul>
      {isLoggedIn ? (
        <li>
          <Link to="/eventboard">Event board</Link>
        </li>
      ) : null}

      {!isLoggedIn ? (
        <li>
          <Link to="/signup">Sign up</Link>
        </li>
      ) : null}
      <li>
        {isLoggedIn ? (
          <Link to="/logout" onClick={handleLogout}>
            Log out
          </Link>
        ) : (
          <Link to="/login">Log in</Link>
        )}
      </li>
    </ul>
  </nav>
);

export default NavBar;

import React, { useEffect, useState, createRef } from 'react';
import axios from 'axios';
import qs from 'qs';

import { validEmailRegex } from '../helpers/formValidation';
import Input from '../Input';

const loginURL = 'http://localhost:5000/login';

//Uncontrolled
class Login extends React.Component {
  state = {
    email: '',
    errors: {
      status: false,
      email: '',
      password: '',
    },
  };

  emailRef = createRef();
  passRef = createRef();

  loginUser(data) {
    return axios.post(loginURL, qs.stringify(data), {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded',
      },
    });
  }

  validateForm = formData => {
    const fieldValidation = (fieldName, value) => {
      const map = {
        email: validEmailRegex.test(value) ? true : 'Email is not valid!',
        password: value.length < 5 ? 'Password must be 5 characters long!' : true,
      };
      return map[fieldName];
    };

    const errors = Object.keys(formData).reduce((acc, key) => {
      acc[key] = fieldValidation(key, formData[key]);
      return acc;
    }, {});

    const isValid = Object.entries(errors).every(([key, value]) => value === true);
    return {
      status: isValid,
      errors,
    };
  };
  handleSubmit = async e => {
    e.preventDefault();
    const formData = {
      email: this.emailRef.current.value,
      password: this.passRef.current.value,
    };

    const { status, errors } = this.validateForm(formData);
    if (status === false) {
      this.setState({ errors });
      return;
    }
    try {
      // TODO If I send back 404, how to get {success: false} ?
      const user = await this.loginUser(formData);
      if (user) {
        window.localStorage.setItem('user', JSON.stringify(formData));
        this.props.handleLogin(formData);
        this.props.history.push('/eventboard');
      }
    } catch (e) {
      this.setState({
        errors: { password: 'Login or password is incorrect, try again' },
      });
    }
  };

  render() {
    return (
      <div className="container">
        <div className="auth-section">
          <h1>Log in Your Scheduler</h1>
          <form className="form" onSubmit={this.handleSubmit}>
            <Input
              name="email"
              type="email"
              placeholder="email address"
              label="Enter your email"
              ref={this.emailRef}
              error={this.state.errors.email}
            />
            <Input
              name="password"
              type="password"
              placeholder="password"
              label="Enter your password"
              ref={this.passRef}
              error={this.state.errors.password}
            />
            <button className="button signup-button">Continue</button>
          </form>
        </div>
      </div>
    );
  }
}

export default Login;

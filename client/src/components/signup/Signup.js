import React, { useEffect, useState, createRef } from 'react';
import axios from 'axios';
import { validEmailRegex } from '../helpers/formValidation';
import Input from '../Input';

import './signup.scss';

// Controlled
class SignUp extends React.Component {
  state = {
    name: null,
    email: null,
    password: null,
    errors: {
      status: false,
      fullName: '',
      email: '',
      password: '',
    },
  };

  async registerUser(user) {
    try {
      const registered = await axios.post('http://localhost:5000/users', user);
      return registered;
    } catch (e) {
      window.console.error(e);
      return null;
    }
  }

  validateField = (fieldName, value) => {
    const map = {
      name: (() => {
        if (value === '' || value.length > 1) {
          return true;
        }
        return 'Full Name must be at least 2 characters!';
      })(),
      email: (() => {
        if (validEmailRegex.test(value) || value === '') {
          return true;
        }
        return 'Email is not valid!';
      })(),
      password: (() => {
        if (value.length > 5 || value === '') {
          return true;
        }
        return 'Password must be 5 characters long!';
      })(),
    };
    return { [fieldName]: map[fieldName] };
  };

  handleChange = event => {
    event.preventDefault();
    const { name, value } = event.target;
    const errors = this.validateField(name, value);
    this.setState({ errors, [name]: value });
  };

  handleSubmit = async e => {
    e.preventDefault();
    const formData = {
      email: this.state.email,
      name: this.state.name,
      password: this.state.password,
    };

    try {
      const result = await this.registerUser(formData);
      if (result) {
        window.localStorage.setItem('user', JSON.stringify(formData));
        this.props.handleLogin(formData);
        this.props.history.push('/eventboard');
      }
    } catch (e) {
      console.log(e);
    }
  };

  render() {
    return (
      <div className="container">
        <div className="auth-section">
          <h1>Sign up with Scheduler for free</h1>
          <form className="form" onSubmit={this.handleSubmit}>
            <Input
              name="email"
              type="email"
              placeholder="email address"
              label="Enter your email"
              onChange={this.handleChange}
              error={this.state.errors.email}
            />
            <Input
              name="name"
              type="text"
              placeholder="John Doe"
              label="Enter your full name"
              onChange={this.handleChange}
              error={this.state.errors.name}
            />
            <Input
              name="password"
              type="password"
              placeholder="password"
              label="Choose a password with at least 8 characters"
              onChange={this.handleChange}
              error={this.state.errors.password}
            />
            <button className="button signup-button">Continue</button>
          </form>
        </div>
      </div>
    );
  }
}

export default SignUp;

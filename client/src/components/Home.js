import React from 'react';

const Home = ({ isLoggedIn, user }) => {
  console.log(user);
  return (
    <div className="home container">
      <h1>
        Welcome home,{''} {isLoggedIn ? user.name : 'sailor!'}
      </h1>
      <img src="https://via.placeholder.com/250" />
    </div>
  );
};

export default Home;

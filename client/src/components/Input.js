import React from 'react';

const Input = React.forwardRef((props, ref) => {
  const { onChange, name, type, placeholder, label, error } = props;

  return (
    <div className={`field ${name}-field`}>
      <h5>{label}</h5>
      <input name={name} type={type} placeholder={placeholder} ref={ref} onChange={onChange} />
      <div className="error-message">{error}</div>
    </div>
  );
});

export default Input;

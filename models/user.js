module.exports = {
  firstName: "Eugene",
  welcome_message:
    "Welcome to my scheduling page. Now go ahead and book an event with me!",
  email: "sherlaimov@gmail.com",
  language: "English",
  dateFormat: "DD/M/YYYY",
  timeFormat: "24h",
  country: "Ukraine",
  timeZone: "Eastern European Time",
  avatar: null,
  myLink: "http://localhost:3000/"
};
